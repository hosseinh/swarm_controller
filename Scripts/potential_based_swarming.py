# import rospy
#
# from geometry_msgs.msg import PoseArray, Twist, Pose
# from swarming.msg import VelocityArray

from math import *
from random import *
import time
import cv2  # OpenCV2 for saving an image
import numpy as np

frequency = 0
initial_time = time.time()


class Swarm:
    def __init__(self):

        ''' Swarm Parameters '''
        self.numbers = 20
        self.max_velocity = 0.5         # m/sec

        self.r_r = 0.2                  # m
        self.r_o = 0.5                  # m
        self.r_a = 0.9                  # m
        self.field_of_perception = 90   # degree

        self.area_width = 5             # m
        self.area_height = 5            # m
        self.margin = 0.3

        self.repulsion_gain = .2
        self.attraction_gain = 0.1
        self.orientation_gain = 0.4

        ''' Initial values'''
        self.position = self.numbers * [[0, 0]]
        self.heading = self.numbers * [0]
        self.heading_vel = self.numbers * [0]
        self.heading_acc = self.numbers * [0]
        self.velocity = self.numbers * [self.max_velocity]
        self.influence_vector_x = self.numbers * [0]
        self.influence_vector_y = self.numbers * [0]


def nothing(x):
    pass


def px(point):
    pixel_scalar = 300
    point_in_pixel = point * pixel_scalar
    return point_in_pixel


def clamp(n, minn, maxn):
    return max(min(maxn, n), minn)


def distance(p, q):
    xi = swarm.position[p][0]
    yi = swarm.position[p][1]
    xii = swarm.position[q][0]
    yii = swarm.position[q][1]
    sq1 = (xi - xii) * (xi - xii)
    sq2 = (yi - yii) * (yi - yii)
    return sqrt(sq1 + sq2)


def is_in_perception_field(p, q):
    alpha = 0
    position_vector_x = swarm.position[q][0] - swarm.position[p][0]
    position_vector_y = swarm.position[q][1] - swarm.position[p][1]
    position_vector_abs = sqrt(position_vector_x**2 + position_vector_y**2)
    if position_vector_abs > 0:
        alpha = acos((position_vector_x * cos(swarm.heading[p]) + position_vector_y * sin(swarm.heading[p])) / position_vector_abs)

    if alpha < 0.5 * swarm.field_of_perception:
        return True
    else:
        return False


def repulsive_boundaries_pt(p):
    delta = 0.5

    # x = cos(swarm.heading[p])
    # y = sin(swarm.heading[p])
    x = 0
    y = 0
    if swarm.position[p][0] < swarm.margin:
        x = x + delta
    if swarm.position[p][0] > swarm.area_width - swarm.margin:
        x -= delta
    if swarm.position[p][1] < swarm.margin:
        y += delta
    if swarm.position[p][1] > swarm.area_height - swarm.margin:
        y -= delta
    ptx = [x, y]
    return ptx
    #swarm.heading[p] = atan2(y, x)


def rotate_list(l, x):
    return l[-x:] + l[:-x]


swarm = Swarm()


''' Initial random values for positions and headings '''
for i in range(swarm.numbers):
    swarm.heading[i] = random() * 2*pi
    swarm.position[i] = [random()*swarm.area_width, random()*swarm.area_height]


width = px(swarm.area_width)
height = px(swarm.area_height)

cv2.namedWindow("swarm", cv2.WINDOW_NORMAL)
cv2.createTrackbar('r_r', 'swarm', 0, swarm.area_width*100, nothing)
cv2.createTrackbar('r_o', 'swarm', 0, swarm.area_width*100, nothing)
cv2.createTrackbar('r_a', 'swarm', 0, swarm.area_width*100, nothing)
cv2.createTrackbar('field_of_perception', 'swarm', 0, 360, nothing)

while cv2.waitKey(1) != 27:
    new_time = time.time()
    time_elapsed = new_time - initial_time
    initial_time = time.time()

    img = np.zeros((height, width, 3), np.uint8)
    swarm.r_r = cv2.getTrackbarPos('r_r', 'swarm') / 100.00
    swarm.r_o = cv2.getTrackbarPos('r_o', 'swarm') / 100.00
    swarm.r_a = cv2.getTrackbarPos('r_a', 'swarm') / 100.00
    swarm.field_of_perception = cv2.getTrackbarPos('field_of_perception', 'swarm')

    a_r = 1/550
    a_a = 1/5500
    a_o = 1/5500

    K_r = 0.03
    K_a = 0.001
    K_o = 0.003

    swarm.influence_vector_x = swarm.numbers * [0]
    swarm.influence_vector_y = swarm.numbers * [0]

    print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
    print(swarm.influence_vector_x[randint(0, 4)])
    print(swarm.influence_vector_y[randint(0, 4)])
    for p in range(swarm.numbers):

        pt_r = [0, 0]
        pt_a = [0, 0]
        pt_o = [0, 0]
        pt = [0, 0]
        T = 0
        for neighbor in range(swarm.numbers):
            if p != neighbor:
                d = distance(p, neighbor)
                r = [swarm.position[neighbor][0] - swarm.position[p][0], swarm.position[neighbor][1] - swarm.position[p][1]]

                pt_r[0] = (-1) * K_r * (r[0]/d) * (pi/2 - atan((d-swarm.r_r)/a_r))
                pt_r[1] = (-1) * K_r * (r[1]/d) * (pi/2 - atan((d-swarm.r_r)/a_r))

                pt_a[0] = K_a * (r[0]/d) * (pi/2 - atan((d-swarm.r_a)/a_a))
                pt_a[1] = K_a * (r[1]/d) * (pi/2 - atan((d-swarm.r_a)/a_a))

                pt_o[0] = K_o * cos(swarm.heading[neighbor]) * (pi/2 - atan((d-swarm.r_o)/a_o))
                pt_o[1] = K_o * sin(swarm.heading[neighbor]) * (pi/2 - atan((d-swarm.r_o)/a_o))

                pt[0] += pt_r[0] + pt_a[0] + pt_o[0]
                pt[1] += pt_r[1] + pt_a[1] + pt_o[1]

                swarm.influence_vector_x[neighbor] += pt[0]
                swarm.influence_vector_y[neighbor] += pt[1]


        print('_________________________________________________________________')
        print(swarm.influence_vector_x[randint(0, 4)])

        print(swarm.influence_vector_y[randint(0, 4)])


        bound_pt = repulsive_boundaries_pt(p)
        pt[0] = pt[0] + bound_pt[0]
        pt[1] = pt[1] + bound_pt[1]

        while abs(swarm.heading[p]) > pi:
            if swarm.heading[p] > pi:
                swarm.heading[p] = swarm.heading[p] - 2*pi
            if swarm.heading[p] < -pi:
                swarm.heading[p] = swarm.heading[p] + 2 * pi

        # if pt[0] == 0 and pt[1] == 0:
        #     pass
        # else:

        #    pt[0] = pt[0] / sqrt(pt[0] ** 2 + pt[1] ** 2)
        #    pt[1] = pt[1] / sqrt(pt[0] ** 2 + pt[1] ** 2)

        cv2.line(img, (int(px(swarm.position[p][0])), int(px(swarm.position[p][1]))),
                 (int(px(swarm.position[p][0]) + 1000 * pt[0]),
                  int(px(swarm.position[p][1]) + 1000 * pt[1])), (200, 23, 200))

        swarm.heading[p] = atan2(pt[1] + 1.0*sin(swarm.heading[p]), pt[0] + 1.0*cos(swarm.heading[p]))

        # swarm.heading_vel[p] = clamp(T, -0.5, 0.5)
    # print(swarm.influence_vector[1])
    for i in range(swarm.numbers):

        # cv2.line(img, (int(px(swarm.position[i][0])), int(px(swarm.position[i][1]))),
        #          (int(px(swarm.position[i][0]) + 100 * swarm.influence_vector_x[i]),
        #           int(px(swarm.position[i][1]) + 100 * swarm.influence_vector_y[i])), (200, 230, 20))

        cv2.line(img, (int(px(swarm.position[i][0])), int(px(swarm.position[i][1]))),
                (int(px(swarm.position[i][0]) + cos(swarm.heading[i])*20) , int(px(swarm.position[i][1]) + sin(swarm.heading[i])*20)), (20, 230, 100))
        cv2.circle(img, (int(px(swarm.position[i][0])), int(px(swarm.position[i][1]))), 4, (0, 0, 220), -1)


    ''' Update positions based on KINEMATIC/DYNAMIC model'''
    ''' Here we use a simple "x = x0 + v*t" kinematic model'''
    dt = time_elapsed
    for i in range(swarm.numbers):
        swarm.position[i][0] += swarm.velocity[i] * dt * cos(swarm.heading[i])
        swarm.position[i][1] += swarm.velocity[i] * dt * sin(swarm.heading[i])

    cv2.imshow("swarm", img)
    cv2.waitKey(1)
    frequency = 1 / time_elapsed
    #print(T)

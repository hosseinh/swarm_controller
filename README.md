swarm_controller codes

2D Swarming based on Vicsek model [ROS Version]:
This code subscribes to positions and orientations of agents and publishes the velocities (cmd_vel1, cmd_vel2, ...) based on Vicsek model [Novel type of phase transition in a system of self-driven particles, 1995]
to run the code:
	
make sure roscore is running

	$ cd ~/[ws]/swarm_controller/Scripts
	$ python vicsek_swarming.py

2D Swarming based on Couzin model [Simulation_OpenCV Version]
This code simulates the motion of swarn agents based on Couzin model [Collective Memory and Spatial Sorting in Animal Groups, 2002]
to run the code:
	
make sure you have installed OpenCV on your system
	
	$ cd ~/[ws]/swarm_controller/Scripts
	$ python couzin_swarming.py
	
2D Swarming based on Potential field [Simulation_OpenCV Version]
This code simulates the motion of swarn agents based on a smooth differentiable potential field.

make sure you have installed OpenCV on your system
	
	$ cd ~/[ws]/swarm_controller/Scripts
	$ python potential_based_swarming.py
	
3D/2D Swarming based on Potential field [Simulation_OpenCV Version (you can see 3D (x y z) swarming projected on (x y) 2D)]
This code simulates the motion of swarn agents based on a smooth differentiable potential field.

make sure you have installed OpenCV on your system
	
	$ cd ~/[ws]/swarm_controller/Scripts
	$ python 3D_potential_based_swarming.py
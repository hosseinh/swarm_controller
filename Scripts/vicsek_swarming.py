#!/usr/bin/env python
import rospy

from geometry_msgs.msg import PoseArray, Twist, Pose
from swarming.msg import VelocityArray

from math import *
from random import *

#
# margin = 0
# max_vel = 0.10
# interaction_radius = 1.00
#
agents_numbers = 3
interaction_radius = 0.6
it_is_not_virgin = False
# agent = []
#
# vel_x = agents_numbers*[0.12]
# vel_y = agents_numbers*[0.12]


class Agent:
    def __init__(self):
        self.x = 7 * [0]
        self.y = 7 * [0]
        self.heading = 7 * [0]
        self.v_x = 7 * [0.00]
        self.v_y = 7 * [0.00]
        self.it_is_not_virgin = False
        self.max_velocity = 0.075

agent = Agent()

for i in range(agents_numbers):
    theta = random() * 2*pi
    agent.v_x[i] = agent.max_velocity * cos(theta)
    agent.v_y[i] = agent.max_velocity * sin(theta)


def distance(xi, yi, xii, yii):
    sq1 = (xi - xii) * (xi - xii)
    sq2 = (yi - yii) * (yi - yii)
    return sqrt(sq1 + sq2)

def callback(msg):
    print('\n')
    print('\n')
    print('\n')
    print('____________________________________________')
    # updating new positions from pose msg
    for j in range(agents_numbers):
        agent.x[j] = msg.poses[j].position.x
        agent.y[j] = msg.poses[j].position.y


    #####################################
    ## HERE THE SWARM CODE TAKES PLACE ##
    #####################################

    for p in range(agents_numbers):
        # searching for other agents in the interaction area of agent[t]
        heading_cos = 0
        heading_sin = 0
        interacting_agents = 0

        for q in range(agents_numbers):
            if distance(agent.x[p], agent.y[p], agent.x[q], agent.y[q]) <= interaction_radius or distance(agent.x[p], agent.x[q], agent.y[p], agent.y[q]) == 0.0:
                interacting_agents = interacting_agents + 1
                heading = atan2(agent.v_y[q], agent.v_x[q])
                heading_cos += cos(heading)
                heading_sin += sin(heading)
        heading_cos = heading_cos / interacting_agents
        heading_sin = heading_sin / interacting_agents

        agent.v_x[p] = heading_cos * agent.max_velocity
        agent.v_y[p] = heading_sin * agent.max_velocity

    # safety check!
    for j in range(agents_numbers):
        if agent.x[j] < 0.1:
            agent.v_x[j] = abs(agent.v_x[j]) + 0.01

        if agent.y[j] < 0.1:
            agent.v_y[j] = abs(agent.v_y[j]) + 0.01

        if agent.x[j] > 2.9:
            agent.v_x[j] = (abs(agent.v_x[j]) * -1) - 0.01

        if agent.y[j] > 2.0:
            agent.v_y[j] = (abs(agent.v_y[j]) * -1) - 0.01

    # publishing new velocities
    for j in range(agents_numbers):
        vel_msg = Twist()
        vel_msg.linear.x = 318 * agent.v_x[j]
        vel_msg.linear.y = 318 * agent.v_y[j]

        print('\n')
        if j == 0:
            pub1.publish(vel_msg)
            print('Agent 1 x velocity: %s' % vel_msg.linear.x)
            print('Agent 1 y velocity: %s' % vel_msg.linear.y)
        if j == 1:
            pub2.publish(vel_msg)
            print('Agent 2 x velocity: %s' % vel_msg.linear.x)
            print('Agent 2 y velocity: %s' % vel_msg.linear.y)
        if j == 2:
            pub3.publish(vel_msg)
            print('Agent 3 x velocity: %s' % vel_msg.linear.x)
            print('Agent 3 y velocity: %s' % vel_msg.linear.y)
        if j == 3:
            pub4.publish(vel_msg)
            print('Agent 4 x velocity: %s' % vel_msg.linear.x)
            print('Agent 4 y velocity: %s' % vel_msg.linear.y)
        if j == 4:
            pub5.publish(vel_msg)
            print('Agent 5 x velocity: %s' % vel_msg.linear.x)
            print('Agent 5 y velocity: %s' % vel_msg.linear.y)
        if j == 5:
            pub6.publish(vel_msg)
            print('Agent 6 x velocity: %s' % vel_msg.linear.x)
            print('Agent 6 y velocity: %s' % vel_msg.linear.y)
        if j == 6:
            pub7.publish(vel_msg)
            print('Agent 7 x velocity: %s' % vel_msg.linear.x)
            print('Agent 7 y velocity: %s' % vel_msg.linear.y)


if __name__ == '__main__':

    rospy.init_node('swarm_controller')
    pub1 = rospy.Publisher('cmd_vel1', Twist, queue_size=10)
    pub2 = rospy.Publisher('cmd_vel2', Twist, queue_size=10)
    pub3 = rospy.Publisher('cmd_vel3', Twist, queue_size=10)
    pub4 = rospy.Publisher('cmd_vel4', Twist, queue_size=10)
    pub5 = rospy.Publisher('cmd_vel5', Twist, queue_size=10)
    pub6 = rospy.Publisher('cmd_vel6', Twist, queue_size=10)
    pub7 = rospy.Publisher('cmd_vel7', Twist, queue_size=10)
    rospy.Subscriber('/position', PoseArray, callback)
    rospy.spin()

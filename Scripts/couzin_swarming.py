# import rospy
#
# from geometry_msgs.msg import PoseArray, Twist, Pose
# from swarming.msg import VelocityArray

from math import *
from random import *
import time
import cv2  # OpenCV2 for saving an image
import numpy as np

frequency = 0
initial_time = time.time()

class Swarm:
    def __init__(self):

        ''' Swarm Parameters '''
        self.numbers = 70
        self.max_velocity = 0.3         # m/sec

        self.r_r = 0.2                  # m
        self.r_o = 0.5                  # m
        self.r_a = 0.9                  # m
        self.field_of_perception = 90   # degree

        self.area_width = 5             # m
        self.area_height = 5            # m
        self.margin = 0.3

        self.repulsion_gain = .2
        self.attraction_gain = 0.1
        self.orientation_gain = 0.4

        ''' Initial values'''
        self.position = self.numbers * [[0, 0]]
        self.heading = self.numbers * [0]
        self.velocity = self.numbers * [self.max_velocity]

class Agent:
    def __init__(self):

        ''' Swarm Parameters '''
        self.numbers = 70
        self.max_velocity = 0.3         # m/sec

        self.r_r = 0.2                  # m
        self.r_o = 0.5                  # m
        self.r_a = 0.9                  # m
        self.field_of_perception = 90   # degree

        self.area_width = 5             # m
        self.area_height = 5            # m
        self.margin = 0.3

        self.repulsion_gain = 0.8
        self.attraction_gain = 0.1
        self.orientation_gain = 0.4

        ''' Initial values'''
        self.position = self.numbers * [[0, 0]]
        self.heading = self.numbers * [0]
        self.velocity = self.numbers * [self.max_velocity]

def nothing(x):
    pass


def px(point):
    pixel_scalar = 300
    point_in_pixel = point * pixel_scalar
    return point_in_pixel


def distance(p, q):
    xi = swarm.position[p][0]
    yi = swarm.position[p][1]
    xii = swarm.position[q][0]
    yii = swarm.position[q][1]
    sq1 = (xi - xii) * (xi - xii)
    sq2 = (yi - yii) * (yi - yii)
    return sqrt(sq1 + sq2)


def is_in_perception_field(p, q):
    alpha = 0
    position_vector_x = swarm.position[q][0] - swarm.position[p][0]
    position_vector_y = swarm.position[q][1] - swarm.position[p][1]
    position_vector_abs = sqrt(position_vector_x**2 + position_vector_y**2)
    if position_vector_abs > 0:
        alpha = acos((position_vector_x * cos(swarm.heading[p]) + position_vector_y * sin(swarm.heading[p])) / position_vector_abs)

    if alpha < 0.5 * swarm.field_of_perception:
        return True
    else:
        return False


def repulsive_boundaries_on(p):
    delta = 0.5


    x = cos(swarm.heading[p])
    y = sin(swarm.heading[p])
    if swarm.position[p][0] < swarm.margin:
        x = x + delta
    if swarm.position[p][0] > swarm.area_width - swarm.margin:
        x -= delta
    if swarm.position[p][1] < swarm.margin:
        y += delta
    if swarm.position[p][1] > swarm.area_height - swarm.margin:
        y -= delta

    swarm.heading[p] = atan2(y, x)


def rotate_list(l, x):
    return l[-x:] + l[:-x]


swarm = Swarm()
agent = Agent()

''' Initial random values for positions and headings '''
for i in range(swarm.numbers):
    swarm.heading[i] = random() * 2*pi
    swarm.position[i] = [random()*swarm.area_width, random()*swarm.area_height]


width = px(swarm.area_width)
height = px(swarm.area_height)

cv2.namedWindow("swarm", cv2.WINDOW_NORMAL)
cv2.createTrackbar('r_r', 'swarm', 0, swarm.area_width*100, nothing)
cv2.createTrackbar('r_o', 'swarm', 0, swarm.area_width*100, nothing)
cv2.createTrackbar('r_a', 'swarm', 0, swarm.area_width*100, nothing)
cv2.createTrackbar('field_of_perception', 'swarm', 0, 360, nothing)

while cv2.waitKey(1) != 27:
    new_time = time.time()
    time_elapsed = new_time - initial_time
    initial_time = time.time()

    img = np.zeros((height, width, 3), np.uint8)
    swarm.r_r = cv2.getTrackbarPos('r_r', 'swarm') / 100.00
    swarm.r_o = cv2.getTrackbarPos('r_o', 'swarm') / 100.00
    swarm.r_a = cv2.getTrackbarPos('r_a', 'swarm') / 100.00
    swarm.field_of_perception = cv2.getTrackbarPos('field_of_perception', 'swarm')

    for i in range(swarm.numbers):
        cv2.line(img, (int(px(swarm.position[i][0])), int(px(swarm.position[i][1]))),
                (int(px(swarm.position[i][0]) + cos(swarm.heading[i])*20) , int(px(swarm.position[i][1]) + sin(swarm.heading[i])*20)), (20, 230, 100))
        cv2.circle(img, (int(px(swarm.position[i][0])), int(px(swarm.position[i][1]))), 4, (0, 0, 220), -1)

    ''' Update positions based on KINEMATIC/DYNAMIC model'''
    ''' Here we use a simple "x = x0 + v*t" kinematic model'''
    dt = time_elapsed
    for i in range(swarm.numbers):
        swarm.position[i][0] += swarm.velocity[i] * dt * cos(swarm.heading[i])
        swarm.position[i][1] += swarm.velocity[i] * dt * sin(swarm.heading[i])

    ''' Apply repulsion effect '''
    for p in range(swarm.numbers):
        repulsion_vector = [0, 0]
        repulsion_counter = 0
        repulsive_boundaries_on(p)
        dm = 0
        for neighbor in range(swarm.numbers):
            if distance(p, neighbor) < swarm.r_r and is_in_perception_field(p, neighbor) and p != neighbor:
                d = distance(p, neighbor) / swarm.r_r
                dm += d
                repulsion_vector[0] += (1/d) * (swarm.position[p][0] - swarm.position[neighbor][0])
                repulsion_vector[1] += (1/d) * (swarm.position[p][1] - swarm.position[neighbor][1])
                repulsion_counter += 1
        if repulsion_counter > 0:
            # swarm.repulsion_gain = 1 / (dm * repulsion_counter)
            repulsion_vector[0] = repulsion_vector[0] / sqrt(repulsion_vector[0] ** 2 + repulsion_vector[1] ** 2)
            repulsion_vector[1] = repulsion_vector[1] / sqrt(repulsion_vector[0] ** 2 + repulsion_vector[1] ** 2)
            swarm.heading[p] = atan2((1-swarm.repulsion_gain) * sin(swarm.heading[p]) + swarm.repulsion_gain * repulsion_vector[1],
                                     (1-swarm.repulsion_gain) * cos(swarm.heading[p]) + swarm.repulsion_gain * repulsion_vector[0])
        else:

            mean_heading_x = 0
            mean_heading_y = 0
            orientation_counter = 0
            repulsion_counter = 0
            attraction_counter = 0
            repulsion_vector = [0, 0]
            attraction_vector = [0, 0]
            d_o = 0
            d_a = 0
            for neighbor in range(swarm.numbers):

                ''' Store orientation effect of neighbor swarms '''
                if distance(p, neighbor) < swarm.r_o and is_in_perception_field(p, neighbor) and swarm.r_o != swarm.r_r:
                    d = (distance(p, neighbor) - swarm.r_r) / (swarm.r_o - swarm.r_r)
                    mean_heading_x += cos(swarm.heading[neighbor])
                    mean_heading_y += sin(swarm.heading[neighbor])
                    orientation_counter += 1


                ''' Store attraction effect of neighbor swarms '''
                if swarm.r_o < distance(p, neighbor) < swarm.r_a and is_in_perception_field(p, neighbor) and p!=neighbor:
                    d = (distance(p, neighbor) - swarm.r_r)
                    attraction_vector[0] += swarm.position[neighbor][0] - swarm.position[p][0]
                    attraction_vector[1] += swarm.position[neighbor][1] - swarm.position[p][1]
                    attraction_counter += 1

            ''' Apply orientation effect '''
            if orientation_counter > 0:
                mean_heading_x = mean_heading_x / orientation_counter
                mean_heading_y = mean_heading_y / orientation_counter
                swarm.heading[p] = atan2(
                    (1 - swarm.orientation_gain) * sin(swarm.heading[p]) + swarm.orientation_gain * mean_heading_y,
                    (1 - swarm.orientation_gain) * cos(swarm.heading[p]) + swarm.orientation_gain * mean_heading_x)

            ''' Apply attraction effect '''
            if attraction_counter > 0:
                attraction_vector[0] = attraction_vector[0] / sqrt(
                    attraction_vector[0] ** 2 + attraction_vector[1] ** 2)
                attraction_vector[1] = attraction_vector[1] / sqrt(
                    attraction_vector[0] ** 2 + attraction_vector[1] ** 2)
                swarm.heading[p] = atan2(
                    (1 - swarm.attraction_gain) * sin(swarm.heading[p]) + swarm.attraction_gain * attraction_vector[1],

                    (1 - swarm.attraction_gain) * cos(swarm.heading[p]) + swarm.attraction_gain * attraction_vector[0])

    cv2.imshow("swarm", img)
    cv2.waitKey(1)
    frequency = 1 / time_elapsed
    print(frequency)

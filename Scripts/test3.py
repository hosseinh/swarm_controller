# import rospy
#
# from geometry_msgs.msg import PoseArray, Twist, Pose
# from swarming.msg import VelocityArray

from math import *
from random import *
import time
import cv2  # OpenCV2 for saving an image
import numpy as np

frequency = 0
initial_time = time.time()

class Swarm:
    def __init__(self):

        ''' Swarm Parameters '''
        self.numbers = 50
        self.max_velocity = 0.5         # m/sec

        self.r_r = 0.2                  # m
        self.r_o = 0.5                  # m
        self.r_a = 0.9                  # m
        self.field_of_perception = 90   # degree

        self.area_width = 5             # m
        self.area_height = 5            # m
        self.margin = 0.3

        self.repulsion_gain = 0.2
        self.attraction_gain = 0.1
        self.orientation_gain = 0.5

        ''' Initial values'''
        self.position = self.numbers * [[0, 0]]
        self.heading = self.numbers * [0]
        self.velocity = self.numbers * [self.max_velocity]


def nothing(x):
    pass


def px(point):
    pixel_scalar = 300
    point_in_pixel = point * pixel_scalar
    return point_in_pixel


def distance(p, q):
    xi = agent.position[p][0]
    yi = agent.position[p][1]
    xii = agent.position[q][0]
    yii = agent.position[q][1]
    sq1 = (xi - xii) * (xi - xii)
    sq2 = (yi - yii) * (yi - yii)
    return sqrt(sq1 + sq2)


def is_in_perception_field(p, q):
    alpha = 0
    position_vector_x = agent.position[q][0] - agent.position[p][0]
    position_vector_y = agent.position[q][1] - agent.position[p][1]
    position_vector_abs = sqrt(position_vector_x**2 + position_vector_y**2)
    if position_vector_abs > 0:
        alpha = acos((position_vector_x * cos(agent.heading[p]) + position_vector_y * sin(agent.heading[p])) / position_vector_abs)

    if alpha < 0.5 * agent.field_of_perception:
        return True
    else:
        return False


def repulsive_boundaries_on(p):
    delta = 0.5


    x = cos(agent.heading[p])
    y = sin(agent.heading[p])
    if agent.position[p][0] < agent.margin:
        x = x + delta
    if agent.position[p][0] > agent.area_width - agent.margin:
        x -= delta
    if agent.position[p][1] < agent.margin:
        y += delta
    if agent.position[p][1] > agent.area_height - agent.margin:
        y -= delta

    agent.heading[p] = atan2(y, x)


def rotate_list(l, x):
    return l[-x:] + l[:-x]


agent = Swarm()

''' Initial random values for positions and headings '''
for i in range(agent.numbers):
    agent.heading[i] = random() * 2*pi
    agent.position[i] = [random()*agent.area_width, random()*agent.area_height]


width = px(agent.area_width)
height = px(agent.area_height)

cv2.namedWindow("swarm", cv2.WINDOW_NORMAL)
cv2.createTrackbar('r_r', 'swarm', 0, agent.area_width*100, nothing)
cv2.createTrackbar('r_o', 'swarm', 0, agent.area_width*100, nothing)
cv2.createTrackbar('r_a', 'swarm', 0, agent.area_width*100, nothing)
cv2.createTrackbar('field_of_perception', 'swarm', 0, 360, nothing)

while cv2.waitKey(1) != 27:
    new_time = time.time()
    time_elapsed = new_time - initial_time
    initial_time = time.time()

    img = np.zeros((height, width, 3), np.uint8)
    agent.r_r = cv2.getTrackbarPos('r_r', 'swarm') / 100.00
    agent.r_o = cv2.getTrackbarPos('r_o', 'swarm') / 100.00
    agent.r_a = cv2.getTrackbarPos('r_a', 'swarm') / 100.00
    agent.field_of_perception = cv2.getTrackbarPos('field_of_perception', 'swarm')

    for i in range(agent.numbers):
        cv2.line(img, (int(px(agent.position[i][0])), int(px(agent.position[i][1]))),
                (int(px(agent.position[i][0]) + cos(agent.heading[i])*20) , int(px(agent.position[i][1]) + sin(agent.heading[i])*20)), (20, 230, 100))
        cv2.circle(img, (int(px(agent.position[i][0])), int(px(agent.position[i][1]))), 10, (0, 0, 220), -1)

    ''' Update positions (KINEMATIC/DYNAMIC MODEL)'''
    dt = time_elapsed
    for i in range(agent.numbers):
        agent.position[i][0] += agent.velocity[i] * dt * cos(agent.heading[i])
        agent.position[i][1] += agent.velocity[i] * dt * sin(agent.heading[i])

    ''' Repulsion effect '''
    for p in range(agent.numbers):
        repulsion_vector = [0, 0]
        repulsion_counter = 0
        repulsive_boundaries_on(p)
        for neighbor in range(agent.numbers):
            if distance(p, neighbor) < agent.r_r and is_in_perception_field(p, neighbor) and p != neighbor:
                repulsion_vector[0] += agent.position[p][0] - agent.position[neighbor][0]
                repulsion_vector[1] += agent.position[p][1] - agent.position[neighbor][1]
                repulsion_counter += 1
        if repulsion_counter > 0:
            repulsion_vector[0] = repulsion_vector[0] / sqrt(repulsion_vector[0] ** 2 + repulsion_vector[1] ** 2)
            repulsion_vector[1] = repulsion_vector[1] / sqrt(repulsion_vector[0] ** 2 + repulsion_vector[1] ** 2)
            agent.heading[p] = atan2((1-agent.repulsion_gain) * sin(agent.heading[p]) + agent.repulsion_gain * repulsion_vector[1],
                                     (1-agent.repulsion_gain) * cos(agent.heading[p]) + agent.repulsion_gain * repulsion_vector[0])
        else:



            mean_heading_x = 0
            mean_heading_y = 0
            orientation_counter = 0
            repulsion_counter = 0
            attraction_counter = 0
            repulsion_vector = [0, 0]
            attraction_vector = [0, 0]
            for neighbor in range(agent.numbers):

                ''' Orientation effect '''
                if distance(p, neighbor) < agent.r_o and is_in_perception_field(p, neighbor):
                    mean_heading_x += cos(agent.heading[neighbor])
                    mean_heading_y += sin(agent.heading[neighbor])
                    orientation_counter += 1

                ''' Attraction effect '''
                if distance(p, neighbor) < agent.r_a and is_in_perception_field(p, neighbor) and p!=neighbor:
                    attraction_vector[0] += agent.position[neighbor][0] - agent.position[p][0]
                    attraction_vector[1] += agent.position[neighbor][1] - agent.position[p][1]
                    attraction_counter += 1
            ''' Apply orientation effect '''
            if orientation_counter > 0:
                mean_heading_x = mean_heading_x / orientation_counter
                mean_heading_y = mean_heading_y / orientation_counter
                agent.heading[p] = atan2(
                    (1 - agent.orientation_gain) * sin(agent.heading[p]) + agent.orientation_gain * mean_heading_y,
                    (1 - agent.orientation_gain) * cos(agent.heading[p]) + agent.orientation_gain * mean_heading_x)

            ''' Apply attraction effect '''
            if attraction_counter > 0:
                attraction_vector[0] = attraction_vector[0] / sqrt(
                    attraction_vector[0] ** 2 + attraction_vector[1] ** 2)
                attraction_vector[1] = attraction_vector[1] / sqrt(
                    attraction_vector[0] ** 2 + attraction_vector[1] ** 2)
                agent.heading[p] = atan2(
                    (1 - agent.attraction_gain) * sin(agent.heading[p]) + agent.attraction_gain * attraction_vector[1],

                    (1 - agent.attraction_gain) * cos(agent.heading[p]) + agent.attraction_gain * attraction_vector[0])

    cv2.imshow("swarm", img)
    cv2.waitKey(1)
    frequency = 1 / time_elapsed
    print(frequency)

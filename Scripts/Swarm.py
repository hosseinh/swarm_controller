#!/usr/bin/env python

import roslib; roslib.load_manifest('visualization_marker_tutorials')
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
import rospy
from math import *
from random import *

topic = 'visualization_marker_array'
publisher = rospy.Publisher(topic, MarkerArray)
rospy.init_node('Test')

space_width = 1000
space_height = 1000
margin = 100
max_vel = 40
interaction_radius = 100
disturbance = 0.6
agents_numbers = 30
agent = []
w_mean = 1
w_lenn = 1

# lennardJones parameters
e_factor = 0.6
s_factor = 60


def distance(p0, p1):
    return sqrt((p0[0] - p1[0]) ** 2 + (p0[1] - p1[1]) ** 2)


def lennard(p0, p1):
    d = distance(p0, p1)
    if d == 0:
        return 0
    else:
        return e_factor * (((s_factor/d)**3) - (1 * ((s_factor/d)**2)))


def show(x0, y0):
    marker = Marker()
    marker.header.frame_id = "/neck"
    marker.type = marker.SPHERE
    marker.action = marker.ADD
    marker.scale.x = 0.2
    marker.scale.y = 0.2
    marker.scale.z = 0.2
    marker.color.a = 1.0
    marker.color.r = 1.0
    marker.color.g = 1.0
    marker.color.b = 0.0
    marker.pose.orientation.w = 1.0
    marker.pose.position.x = x0 / 100
    marker.pose.position.y = y0 / 100
    marker.pose.position.z = 0

    markerArray.markers.append(marker)


# assigning agents with random initial setup values
for i in range(agents_numbers):
    # agent(positionX,positionY,velocity,Orientation)
    agent.append([
        randint(0 + margin, space_width - margin),
        randint(0 + margin, space_height - margin),
        max_vel * 2 * (random() - 0.5),
        max_vel * 2 * (random() - 0.5),
        0.0,
        0.0,
        0.0,
        0.0

    ])

rate = rospy.Rate(30)


while not rospy.is_shutdown():

    # Recreate publishing list
    markerArray = MarkerArray()

    # Update agents Positions
    for j in range(agents_numbers):
        x = agent[j][0]
        y = agent[j][1]
        v = agent[j][2]
        o = agent[j][3]
        # updating new positions
        x += v * cos(o)
        y += v * sin(o)
        agent[j][0] = x
        agent[j][1] = y

        show(agent[j][0], agent[j][1])

    # Search for other agents in the interaction area of agent[t]
    for t in range(agents_numbers):
        # searching for other agents in the interaction area of agent[t]
        mean_x = 0
        mean_y = 0

        mean_pt_x = 0
        mean_pt_y = 0
        interacting_agents = 0
        for q in range(agents_numbers):
            if distance(agent[t], agent[q]) <= interaction_radius:
                interacting_agents += 1
                mean_x += agent[q][2]
                mean_y += agent[q][3]

        agent[t][4] = mean_x / interacting_agents
        agent[t][5] = mean_y / interacting_agents

        for p in range(agents_numbers):
            potential = lennard(agent[t], agent[p])
            if potential == 0:
                agent[t][6] = 0
                agent[t][7] = 0
            else:
                mean_pt_x += -1 * potential * (agent[p][0] - agent[t][0]) / (distance(agent[p], agent[t]))
                mean_pt_y += -1 * potential * (agent[p][1] - agent[t][1]) / (distance(agent[p], agent[t]))

            agent[t][6] = mean_pt_x
            agent[t][7] = mean_pt_y

    # agent[t][4] is the new value for agent[t][3]
    # but can't update it in the above loop because it will affect other agent's angle calculation
    for i in range(agents_numbers):
        agent[i][2] = w_mean * agent[i][4] + w_lenn * agent[i][6] + (random() - 0.5) * disturbance
        agent[i][3] = w_mean * agent[i][5] + w_lenn * agent[i][7] + (random() - 0.5) * disturbance

    # Renumber the marker IDs
    id = 0
    for m in markerArray.markers:
        m.id = id
        id += 1

    # Publish the MarkerArray
    publisher.publish(markerArray)

    rate.sleep()
